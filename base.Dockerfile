# This file is part of gentoo-repoman-container.

# gentoo-repoman-container is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, version 3. 

# gentoo-repoman-container is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU General Public License for more details. 

# You should have received a copy of the GNU General Public License 
# along with scripts.  If not, see <https://www.gnu.org/licenses/>. 

# Original author: Alfred Wingate (parona@protonmail.com) 
# Copyright (c) 2021, src_prepare group 
# Licensed under the GNU GPL v3 License


FROM gentoo/portage:latest as portage
FROM gentoo/stage3:amd64-openrc

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

ARG EMERGE_DEFAULT_OPTS=""
ARG FEATURES=""
ARG GLOBAL_USE=""
ARG PORTAGE_BINHOST=""

RUN echo "USE=\""${GLOBAL_USE}"\"" >> /etc/portage/make.conf \
&& emerge app-portage/repoman dev-vcs/git app-portage/gentoolkit dev-util/pkgcheck
